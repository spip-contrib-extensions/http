<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction appelée par le pipeline.
 */
function http_autoriser($flux) {
	return $flux;
}

/**
 * Voir l'index contenant à priori les collections disponibles : tout le monde peut voir l'index par défaut.
 *
 * @param string          $faire   L'action : `get_index`
 * @param null|string     $quoi    Le type d'objet ou nom de table : chaine vide
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : inutilisé
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_get_index_dist($faire, $quoi, $id, $qui, $options) {
	return true;
}

/**
 * Voir une liste d'objet par HTTP : tout le monde a le droit de voir des listes.
 *
 * @param string          $faire   L'action : `get_collection`
 * @param null|string     $quoi    Le type d'objet ou nom de table : identifiant de la collection
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : inutilisé
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_get_collection_dist($faire, $quoi, $id, $qui, $options) {
	return true;
}

/**
 * Voir un objet par HTTP : on redirige vers la fonction pour voir l'objet.
 *
 * @param string          $faire   L'action : `get_ressource`
 * @param null|string     $quoi    Le type d'objet ou nom de table : identifiant de la collection
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : identifiant de la ressource de la collection
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_get_ressource_dist($faire, $quoi, $id, $qui, $options) {
	return autoriser('voir', $quoi, $id, $qui, $options);
}

//
/**
 * Ajouter un objet par HTTP : on redirige vers la création de l'objet.
 *
 * @param string          $faire   L'action : `get_collection`
 * @param null|string     $quoi    Le type d'objet ou nom de table : identifiant de la collection
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : inutilisé
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_post_collection_dist($faire, $quoi, $id, $qui, $options) {
	return autoriser('creer', $quoi, $id, $qui, $options);
}

/**
 * Modifier un objet par HTTP : on redirige vers la modification.
 *
 * @param string          $faire   L'action : `put_ressource`
 * @param null|string     $quoi    Le type d'objet ou nom de table : identifiant de la collection
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : identifiant de la ressource de la collection
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_put_ressource_dist($faire, $quoi, $id, $qui, $options) {
	return autoriser('modifier', $quoi, $id, $qui, $options);
}

// Supprimer un objet par HTTP : soit il existe une autorisation de suppression soit sinon l'institution
/**
 * Voir un objet par HTTP : on redirige vers la fonction pour voir l'objet.
 *
 * @param string          $faire   L'action : `delete_ressource`
 * @param null|string     $quoi    Le type d'objet ou nom de table : identifiant de la collection
 * @param null|int|string $id      Id de l'objet sur lequel on veut agir : identifiant de la ressource de la collection
 * @param null|array|int  $qui     L'initiateur de l'action:
 *                                 - si null on prend alors visiteur_session
 *                                 - un id_auteur (on regarde dans la base)
 *                                 - un tableau auteur complet, y compris [restreint]
 * @param null|array      $options Tableau d'options sous forme de tableau associatif : inutilisé
 *
 * @return bool `true`si l'auteur est autorisée à exécuter l'action, `false` sinon.
 */
function autoriser_delete_ressource_dist($faire, $quoi, $id, $qui, $options) {
	return autoriser('supprimer', $quoi, $id, $qui, $options)
		or autoriser('instituer', $quoi, $id, $qui, $options);
}
