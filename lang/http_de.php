<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/http?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_401_message' => 'Sie sind nicht berechtigt, auf diese Ressource zuzugreifen.',
	'erreur_404_message' => 'Die angeforderte Ressource wurde nicht gefunden.',
	'erreur_404_titre' => 'Nicht gefunden.',
	'erreur_415_message' => 'Die URL der Anfrage war zu lang oder falsch formuliert.',
	'erreur_415_titre' => 'Unbekanntes Format',
];
