<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/http?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// E
	'erreur_401_message' => 'You do not have permission to access this resource.',
	'erreur_404_message' => 'You requested a resource that does not exist or has not been found.', # MODIF
	'erreur_404_titre' => 'The resource was not found',
	'erreur_415_message' => 'Your request is in an unknown format not supported by this server.',
	'erreur_415_titre' => 'Unknown format',
];
