<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/http.git

return [

	// E
	'erreur_401_message' => 'Vous n’avez pas le droit d’accéder à cette ressource.',
	'erreur_401_titre' => 'Accés non autorisé',
	'erreur_404_message' => 'Vous avez demandé une ressource qui n’existe pas ou qui n’a pas été trouvée.',
	'erreur_404_titre' => 'La ressource n’a pas été trouvée',
	'erreur_415_message' => 'Votre requête est dans un format inconnu, non supporté par ce serveur.',
	'erreur_415_titre' => 'Format inconnu',
];
