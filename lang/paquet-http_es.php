<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-http?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// H
	'http_description' => 'Este plugin provee una API para que otros plugins puedan implementar más fácilmente servicios que utilizan los  métodos HTTP.',
	'http_nom' => 'Servidor HTTP abstracto',
	'http_slogan' => 'Gestión abstracta de los métodos HTTP.',
];
