<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/http.git

return [

	// H
	'http_description' => 'Ce plugin fournit une API pour que d’autres plugins puissent implémenter plus facilement des services utilisant les méthodes HTTP.',
	'http_nom' => 'Serveur HTTP abstrait',
	'http_slogan' => 'Gestion abstraite des méthodes HTTP.',
];
