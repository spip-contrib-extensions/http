<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-http?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// H
	'http_description' => 'Deze plugin voorziet in een API waarmee andere plugins op eenvoudige wijze HTTP services kunnen implementeren.',
	'http_nom' => 'Vereenvoudigd HTTP-beheer',
	'http_slogan' => 'Eenvoudig beheren van HTTP methodes.',
];
